from rest_framework import serializers
from .models import Article, Category, Commentaire


class ArticleSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Article
        fields = ('id', 'title', 'summary', 'content')


class CategorySerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Category
        fields = ('id', 'name', 'image_url')


class CommentaireSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Commentaire
        fields = ('id', 'article', 'author_name', 'content', 'creation_date')

from django.conf import settings
from django.conf.urls.static import static
from django.urls import path, include
from . import views
from rest_framework import routers
from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView

router = routers.DefaultRouter()
router.register(r'articlesss', views.ArticleViewSet)
router.register(r'category', views.CategoryViewSet)
router.register(r'commentaires', views.CommentaireViewSet)

''' RedirectView.as_view(url='../admin/')'''
urlpatterns = [
    path('', views.index, name='index'),
    path('categories/', views.categorie, name='categorie'),
    path('commentaires/', views.commentaire, name='commentaire'),
    path('<int:id>', views.article_details, name='article_detail'),
    path('api/', include(router.urls)),
    path('auth/', include('rest_framework.urls')),
    path('categories_/', views.category_index, name='categories'),
    path('categories_/<int:pk>/', views.category_detail, name='categories'),
    path('commentaires_/<int:pk>/', views.commentaire_detail, name='commentaire'),
    path('commentaires_/', views.commentaire_index, name='commentaire'),

]+static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
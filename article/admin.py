from django.contrib import admin
from article.models import Category, Article, Commentaire


class ArticleAdmin(admin.ModelAdmin):
    list_display = ('id','title', 'slug', 'created_at')
    fields = [('title', 'slug', 'photo'), 'category', 'summary', 'content', 'publish_time']
    list_filter = ('created_at', 'category')
    list_display_links = ('title', 'created_at')
    list_editable = ('slug',)
    list_per_page = 4
    ''' mila asiana search fields category '''
    autocomplete_fields = ('category',)
    readonly_fields = ('id',)
    search_fields = ('title','category__name')


class CategoryAdmin(admin.ModelAdmin):
    list_display = ('name',)
    fields = ['name', 'image_url']
    list_filter = ('name',)
    search_fields = ('name',)


class CommentaireAdmin(admin.ModelAdmin):
    list_display = ('article','author_name')
    fields = [('article','author_name'),'content']
    list_filter = ('article',)
    search_fields = ('author_name',)


admin.site.register(Article, ArticleAdmin)
admin.site.register(Category, CategoryAdmin)
admin.site.register(Commentaire, CommentaireAdmin)

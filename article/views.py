from django.shortcuts import render, get_object_or_404
from rest_framework.decorators import api_view

from .models import Article, Category, Commentaire
from rest_framework import viewsets,status
from .serializers import ArticleSerializer, CategorySerializer, CommentaireSerializer
from django.http import JsonResponse, HttpResponse
from rest_framework.parsers import JSONParser
from rest_framework.response import Response
from django.views.decorators.csrf import csrf_exempt
from rest_framework.views import APIView


class ArticleViewSet(viewsets.ModelViewSet):
    queryset = Article.objects.all()
    serializer_class = ArticleSerializer


class CategoryViewSet(viewsets.ModelViewSet):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer


class CommentaireViewSet(viewsets.ModelViewSet):
    queryset = Commentaire.objects.all()
    serializer_class = CommentaireSerializer


'''@csrf_exempt
def category_index(request):

    if request.method == 'GET':
        categories = Category.objects.all()
        serializer = CategorySerializer(categories, many=True)
        return JsonResponse(serializer.data, safe=False)

    if request.method == 'POST':
        data = JSONParser().parse(request)
        serializer = CategorySerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data, status=201)
        return JsonResponse(serializer.errors, status=400)
'''

'''@csrf_exempt
def category_detail(request,pk):

    try:
        category = Category.objects.get(pk=pk)
    except Category.DoesNotExist:
        return JsonResponse({"error":f"la catégorie avec l'id {pk} n'existe pas"}, status=404)

    if request.method == 'GET':
        serializer = CategorySerializer(category)
        return JsonResponse(serializer.data)

    if request.method =='PUT':
        data = JSONParser().parse(request)
        serializer = CategorySerializer(category, data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data, status=200)
        return JsonResponse(serializer.errors, status=400)

    if request.method == 'DELETE':
        category.delete()
        return JsonResponse({"response":f"la catégorie avec l'id {pk} a été supprimée"},status=204)
'''


@api_view(['GET','PUT','DELETE', 'POST'])
def category_detail(request,pk):

    try:
        category = Category.objects.get(pk=pk)
    except Category.DoesNotExist:
        return Response({"error":f"la catégorie avec l'id {pk} n'existe pas :,("}, status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = CategorySerializer(category, context={'request':request})
        return Response(serializer.data)

    if request.method =='PUT':
        data = request.data
        serializer = CategorySerializer(category, data=data, context={'request':request})
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_200_OK)

    if request.method == 'DELETE':
        category.delete()
        return Response({"response":f"la catégorie avec l'id {pk} a été supprimée"},status=status.HTTP_200_OK)


@api_view(['GET','PUT','DELETE', 'POST'])
def commentaire_detail(request,pk):

    try:
        commentaire = Commentaire.objects.get(pk=pk)
    except Commentaire.DoesNotExist:
        return Response({"error":f"le commentaire avec l'id {pk} n'existe pas :,("}, status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = CommentaireSerializer(commentaire, context={'request':request})
        return Response(serializer.data)

    if request.method =='PUT':
        data = request.data
        serializer = CommentaireSerializer(commentaire, data=data, context={'request':request})
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_200_OK)

    if request.method == 'DELETE':
        commentaire.delete()
        return Response({"response":f"le commentaire avec l'id {pk} a été supprimée"},status=status.HTTP_200_OK)


@api_view(['GET','POST'])
def category_index(request):

    if request.method == 'GET':
        categories = Category.objects.all()
        serializer = CategorySerializer(categories, many=True, context={'request':request})
        return Response(serializer.data)

    if request.method == 'POST':
        data = request.data
        serializer = CategorySerializer(data=data,context={'request':request})
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET','POST'])
def commentaire_index(request):

    if request.method == 'GET':
        commentaires = Commentaire.objects.all()
        serializer = CommentaireSerializer(commentaires, many=True, context={'request':request})
        return Response(serializer.data)

    if request.method == 'POST':
        data = request.data
        serializer = CommentaireSerializer(data=data, context={'request':request})
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


def index(request):
    """View function for home page of site."""

    num_articles = Article.objects.all().count()
    num_categories = Category.objects.all().count()
    articles = Article.objects.all()
    mess = "Bienvenue dans mon blog"
    urladmin ="/admin"

    context = {
        'num_articles': num_articles,
        'num_categories': num_categories,
        'articles_list': articles,
        'mess': mess,
        'urladmin': urladmin
    }

    return render(request, 'index.html', context=context)


def categorie(request):
    """View function for home page of site."""

    Categories = Category.objects.all()

    context = {
        'categories': Categories,
    }

    return render(request, 'categorie.html', context=context)

def commentaire(request):
    """View function for home page of site."""

    Commentaires = Commentaire.objects.filter(article__title__contains='no')

    context = {
        'commentaires': Commentaires,
    }

    return render(request, 'commentaire.html', context=context)

def article_details(request, id):

    a="a"
    '''art = get_object_or_404(Article, pk=id)'''
    art = get_object_or_404(Article, id=id)
    context = {
        'article': art,
    }

    return render(request, 'article_detail.html', context=context)

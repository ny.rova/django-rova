from django.apps import AppConfig


class JwauthConfig(AppConfig):
    name = 'jwauth'

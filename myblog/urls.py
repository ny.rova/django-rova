from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static
from django.views.generic import RedirectView


urlpatterns = [
    path('admin/', admin.site.urls),
    path('articles/', include('article.urls')),
    path('', RedirectView.as_view(url='articles/')),
    path('auth/', include('jwauth.urls'), name='jwtauth')

]
